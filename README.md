# README #

### Related LGPLv3 libs (and their source code location) in HGMC application ###

* [Qt Essentials v5.10.1](http://doc.qt.io/qt-5/qtmodules.html): Source code can be found on [https://github.com/qt/qt5/releases/tag/v5.10.1](https://github.com/qt/qt5/releases/tag/v5.10.1)
* [Qt Add-Ons v5.10.1](http://doc.qt.io/qt-5/qtmodules.html): Source code can be found on [https://github.com/qt/qt5/releases/tag/v5.10.1](https://github.com/qt/qt5/releases/tag/v5.10.1)
* [QtAndroidSerialPort project](https://bitbucket.org/hiveground/android-serial-port-lib): Source code can be found on [https://bitbucket.org/hiveground/android-serial-port-lib/src](https://bitbucket.org/hiveground/android-serial-port-lib/src)

### How to pack the modified LGPLv3 libraries used by HGMC back into new APK package ###

* Acquire HGMC application from your android device by following the steps below:
	* enable developer and usb debugging on your android device
	* connect your android device to your machine
	* On the machine, use android 'adb' tool to find a path to the application's apk on the device by using this command 'adb shell pm path missioncontrol.hiveground.com'
	* On the machine, use android 'adb' tool to pull the apk package by using this command 'adb pull <path to apk returned from previous step>'
* Unzip the package (you can just change the extension from apk to zip)
* Replace the modified libraries
	* if it's a native library (*.so), you can replace it in lib/armeabi-v7a/ folder.
	* if it's a java library (all java libs are in *.dex files), please follow the steps below
		* download tool dex2jar (or use other equivalent dex packaging tools)
		* use command d2j-dex2jar (or other equivalent tools) to unpack the *.dex into a jar file
		* jar file can be unpacked by many archive managers pretty much like zip file
		* replace all the modified classes that you want into the package
		* pack everything back into a new jar file
		* use command d2j-jar2dex (or other equivalent tools) to pack the new jar file into new *.dex files
* Zip all items back together
* Use the tool apksigner to sign the package back with your key (the key can be generated using the tools such as keytool). Command example: ./apksigner sign --ks /path/to/key/youkey.keystore --min-sdk-version 16 --out /path/to/output/signed-apk.apk  /path/to/input/package.zip

